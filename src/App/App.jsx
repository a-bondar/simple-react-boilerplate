import React from 'react';
import { hot } from 'react-hot-loader';

import styles from './App.css';

const App = () => <h1 className={styles.app}>Hello</h1>;

export default hot(module)(App);
